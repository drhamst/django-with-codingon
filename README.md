Linux Shell Commands<br/>
<br/>
-run server<br/>
python3 manage.py runserver 0.0.0.0:8000<br/>
<br/>
-run server on background<br/>
nohup python3 manage.py runserver 0.0.0.0:8000 &<br/>
-kill server on background<br/>
ps -ef | grep ':8000'<br/>
kill - 9 type:pid<br/>
<br/>
-make new app<br/>
django-admin startapp vip_user<br/>
<br/>
-magrates...<br/>
python3 manage.py makemigrations vip_user(내부결제, 커밋, DB구조 형상관리)<br/>
python3 manage.py migrate(푸쉬, 실서버 반영)<br/>
<br/>
-make django admin account<br/>
python3 manage.py createsuperuser<br/>
<br/>
