# Generated by Django 3.2.4 on 2021-06-11 07:10

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('productid', models.CharField(max_length=64, verbose_name='productid')),
                ('productname', models.CharField(max_length=5, verbose_name='productname')),
                ('price', models.CharField(max_length=64, verbose_name='price')),
                ('registered', models.DateTimeField(auto_now_add=True, verbose_name='registered')),
            ],
        ),
    ]
