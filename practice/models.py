from django.db import models

# Create your models here.

class Product(models.Model):
	productid=models.CharField(max_length=5, verbose_name='productid')
	productname=models.CharField(max_length=30, verbose_name='productname')
	price=models.CharField(max_length=20, verbose_name='price')
	registered=models.DateTimeField(auto_now_add=True, verbose_name='registered')

class PracticeUser(models.Model):
	user_id=models.CharField(max_length=10, verbose_name='USER_ID' )
	name=models.CharField(max_length=30, verbose_name='Name')
	password=models.CharField(max_length=20, verbose_name='Password')
	registered=models.DateTimeField(auto_now_add=True, verbose_name='registered')