from django.shortcuts import render
from .models import Product
from .models import PracticeUser
from django.shortcuts import redirect

# Create your views here.

def hello(req):
    if req.method == "POST" :
        product = Product(productid=req.POST.get('productid'), productname=req.POST.get('productname'), price=req.POST.get('price'))
        product.save()
        joyeong = Product.objects.all()
        return render(req, 'receive2.html', {'sukkyung':joyeong})
    else :
        return render(req, 'hello.html', {})
    
def sign_in(req):

    if req.method == "POST" :

        userid = req.POST.get('user_id')
        password = req.POST.get('password')
        is_sign_out = req.POST.get('sign_out')
        data = {}

        # 로그아웃#####################
        if is_sign_out == 'true':
            del req.session['user']
            return render(req, 'sign_in.html', {})


        # 로그인#####################
        if not ( userid and password ):
            data['error'] = '모든 값을 입력해주세요.'
            return render(req, 'sign_in.html', data)
        else:
            try:
                user = PracticeUser.objects.get(user_id=userid)
            except PracticeUser.DoesNotExist:
                data['error'] = 'Please, check your id and password'
                return render(req, 'sign_in.html', data)
            if user.password == password:
                req.session['user'] = userid
                return render(req, 'sign_in.html', {})
            else:
                data['error'] = 'Please, check your id and password'
                return render(req, 'sign_in.html', data)

    else:
        users = PracticeUser.objects.all()
        return render(req, 'sign_in.html', {'users': users})
    

def sign_up(req):
    if req.method == "POST" :
        practice_user = PracticeUser(user_id=req.POST.get('user_id'), name=req.POST.get('name'), password=req.POST.get('password'))
        practice_user.save()
        data = {}
        data['Sign_Up_Complete'] = 'Sign Up Complete!'
        return render(req, 'sign_up.html', data)
    else :
        return render(req, 'sign_up.html', {})

def account_info(req):
    if req.method == "POST" :
        practice_user = PracticeUser(user_id=req.POST.get('user_id'), name=req.POST.get('name'), password=req.POST.get('password'))
        practice_user.save()
        data = {}
        data['Sign_Up_Complete'] = 'Sign Up Complete!'
        return render(req, 'sign_up.html', data)
    else :
        session_user_id = req.session.get('user', False)
        if session_user_id :
            user_info = PracticeUser.objects.get(user_id=session_user_id)
            return render(req, 'account_info.html', {'user_info': user_info})
        else:
            return redirect('./signin')

        
def form(req):
    if req.method == "POST" :
        name=req.POST.get('name')
        gender=req.POST.get('gender')
        year=req.POST.get('year')
        month=req.POST.get('month')
        day=req.POST.get('day')

        return render(req, 'form.html', {
            'name':name,
            'gender':gender,
            'year':year,
            'month':month,
            'day':day
            })
    else :
        return render(req, 'form.html', {})