from django.urls import path
from . import views

urlpatterns = [
	path( "hello", views.hello, name = "첫페이지" ),
	path( "signin", views.sign_in, name = "Sign In" ),
	path( "signup", views.sign_up, name = "Sign Up" ),
	path( "accountinfo", views.account_info, name = "Account Info" ),
	path( "form", views.form, name = "Form" )
]
