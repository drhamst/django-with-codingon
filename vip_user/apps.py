from django.apps import AppConfig


class VipUserConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'vip_user'
