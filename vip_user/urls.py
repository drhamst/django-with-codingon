from django.urls import path
from . import views

urlpatterns = [
	path( "yes", views.intro, name = "첫페이지" ),
    path( "send", views.send, name = "send" ),
    path( "receive", views.receive, name = "receive" ),
]

